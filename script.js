document.addEventListener('DOMContentLoaded', function () {

    const nextBtn = document.querySelector('.next');
    const prevBtn = document.querySelector('.prev');

    const slides = document.querySelectorAll('.slide');
    const arrSlides = Array.from(slides);

    console.log(arrSlides)

    let activeSlide = 0;

    nextBtn.addEventListener('click', () => {
        activeSlide++

        if (activeSlide > arrSlides.length - 1) {
            activeSlide = 0;
        }

        setActiveSlide();
    })

    prevBtn.addEventListener('click', () => {
        activeSlide--

        if (activeSlide < 0) {
            activeSlide = arrSlides.length - 1;
        }

        setActiveSlide();
    })

    function setActiveSlide() {
        arrSlides.forEach((element) =>
            element.classList.remove('active'));

        arrSlides[activeSlide].classList.add('active');

    }

    const numberSlide = document.getElementById('input_value');
    const btnNumber = document.getElementById('btn_value');

    btnNumber.addEventListener('click', setNumberImages);

    function setNumberImages() {
        if(numberSlide.value > 10){
            alert('non ci sono più di 10 immagini');
        } else {
            arrSlides.length = numberSlide.value;
        for (i = 0; i < arrSlides.length; i++) {
            console.log(arrSlides[i])
        }}
        numberSlide.value = '';
    }
})
